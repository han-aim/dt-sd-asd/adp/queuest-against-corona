use super::beroepsgroep::Beroepsgroep;
pub use chrono::{DateTime, Utc};
use core::cmp::Ordering;

// Een `Testaanvraag` wordt eerst geordend d.m.v. derive op `nederlander` (dus indirect `nederlander.beroepsgroep`), daarna pas op `datumtijd`. Dus een testaanvraag van een Nederlander met een kritiekere beroepsgroep is altijd *hoger* dan een evt. andere testaanvraag die al eerder gedaan is door een Nederlander met een minder kritieke beroepsgroep.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Testaanvraag {
    pub beroepsgroep: Beroepsgroep,
    pub datumtijd: DateTime<Utc>,
}

impl PartialOrd for Testaanvraag {
    fn partial_cmp(&self, other: &Testaanvraag) -> Option<Ordering> {
        return Some(self.cmp(other));
    }
}

// Handmatig geïmplementeerd, om *vroegere* `datumtijd` een *hogere prioriteit* te geven.
impl Ord for Testaanvraag {
    fn cmp(&self, other: &Self) -> Ordering {
        let ordering_beroepsgroep = self.beroepsgroep.cmp(&other.beroepsgroep);
        return if ordering_beroepsgroep == Ordering::Equal {
            self.datumtijd.cmp(&other.datumtijd).reverse()
        } else {
            ordering_beroepsgroep
        };
    }
}

#[cfg(test)]
mod tests {
    use super::super::{
        beroepsgroep::Beroepsgroep,
        testaanvraag::{DateTime, Testaanvraag, Utc},
    };
    use chrono::Duration;
    #[test]
    fn test_ordering_1() {
        let datetime_nu: DateTime<Utc> = Utc::now();
        let testaanvraag_1 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg1,
            datumtijd: datetime_nu - Duration::days(14),
        };
        let testaanvraag_2 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg3,
            datumtijd: datetime_nu - Duration::days(10),
        };
        assert!(testaanvraag_2 > testaanvraag_1);
    }

    #[test]
    fn test_ordering_2() {
        let datetime_nu: DateTime<Utc> = Utc::now();
        let testaanvraag_1 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg3,
            datumtijd: datetime_nu - Duration::days(14),
        };
        let testaanvraag_2 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg3,
            datumtijd: datetime_nu - Duration::days(10),
        };
        assert!(testaanvraag_1 > testaanvraag_2);
    }

    #[test]
    fn test_ordering_3() {
        let datetime_nu: DateTime<Utc> = Utc::now();
        let testaanvraag_1 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg2,
            datumtijd: datetime_nu,
        };
        let testaanvraag_2 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg2,
            datumtijd: datetime_nu,
        };
        assert!(!(testaanvraag_1 > testaanvraag_2));
        assert!(!(testaanvraag_1 < testaanvraag_2));
        assert!(testaanvraag_1 == testaanvraag_2);
    }
}
