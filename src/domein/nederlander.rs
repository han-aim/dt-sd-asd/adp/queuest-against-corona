pub use chrono::{DateTime, Utc};

pub type Id = String;

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct Nederlander {
    id: Id,
}

impl Nederlander {
    #[must_use]
    pub fn new(id: &str) -> Self {
        return Nederlander { id: Id::from(id) };
    }
}
