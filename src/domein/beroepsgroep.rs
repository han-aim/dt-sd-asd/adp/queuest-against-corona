// `derive` implementeert een bepaalde trait (soort interface uit Java) automatisch, gebaseerd op alle velden, [van boven naar onder](https://doc.rust-lang.org/std/cmp/trait.PartialEq.html#derivable).
// `PartialEq` is om gelijkheid en `PartialOrd` om volgorde te bepalen.
#[derive(Debug, PartialOrd, PartialEq, Ord, Eq, Clone, Copy, Hash)]
pub enum Beroepsgroep {
    /// Beroepsgroep met hoogste prioriteit
    Bg1,
    Bg2,
    Bg3,
}
