use super::domein::{nederlander::Nederlander, testaanvraag::Testaanvraag};
use priority_queue::PriorityQueue;

#[derive(Debug, Default, Clone)]
pub struct Prioriteitswachtrij<'a> {
    priorityqueue: PriorityQueue<&'a Nederlander, &'a Testaanvraag>,
}

impl<'a> Prioriteitswachtrij<'a> {
    #[must_use]
    pub fn new() -> Self {
        return Prioriteitswachtrij {
            priorityqueue: PriorityQueue::new(),
        };
    }

    pub fn enqueue(
        &'_ mut self,
        nederlander: &'a Nederlander,
        testaanvraag: &'a Testaanvraag,
    ) -> Option<&'a Testaanvraag> {
        // Vervroeg de `datumtijd` van een testaanvraag die al in de wachtrij staat als de nieuwe testaanvraag een vroegere `datumtijd` heeft (om administratieve achterstanden aan te kunnen), maar voeg geen nieuwe testaanvraag toe.
        return self.priorityqueue.push_increase(nederlander, testaanvraag);
    }

    #[must_use]
    pub fn dequeue(&mut self) -> Option<&Nederlander> {
        //`.0` Verkrijg het eerste element uit de tupel `(item, prioriteit)`.
        return self.priorityqueue.pop().map(|item| item.0);
    }
}

#[cfg(test)]
mod tests {
    use super::super::domein::{
        beroepsgroep::Beroepsgroep,
        nederlander::Nederlander,
        testaanvraag::{DateTime, Testaanvraag, Utc},
    };
    use super::Prioriteitswachtrij;
    use chrono::Duration;

    #[test]
    fn test_priorityqueue() {
        let nederlander_1 = Nederlander::new("NL1");
        let nederlander_2 = Nederlander::new("NL2");
        let nederlander_3 = Nederlander::new("NL3");
        let nederlander_4 = Nederlander::new("NL4");
        let datetime_nu: DateTime<Utc> = Utc::now();
        let testaanvraag_1 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg1,
            datumtijd: datetime_nu - Duration::days(14),
        };
        let testaanvraag_2 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg3,
            datumtijd: datetime_nu - Duration::days(10),
        };
        let testaanvraag_3 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg2,
            datumtijd: datetime_nu - Duration::days(5),
        };
        let testaanvraag_4 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg3,
            datumtijd: datetime_nu - Duration::days(3),
        };
        let mut prioriteitswachtrij = Prioriteitswachtrij::new();
        assert_eq!(
            prioriteitswachtrij.enqueue(&nederlander_1, &testaanvraag_1),
            None
        );
        assert_eq!(
            prioriteitswachtrij.enqueue(&nederlander_2, &testaanvraag_2),
            None
        );
        assert_eq!(
            prioriteitswachtrij.enqueue(&nederlander_3, &testaanvraag_3),
            None
        );
        assert_eq!(
            prioriteitswachtrij.enqueue(&nederlander_4, &testaanvraag_4),
            None
        );
        // Van `nederlander_1`, die al een testaanvraag heeft uitstaan, maar hem later opnieuw doet.
        let testaanvraag_5 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg1,
            datumtijd: datetime_nu - Duration::days(2),
        };
        assert!(testaanvraag_1 > testaanvraag_5);
        // Testaanvraag 5 wordt niet behandeld, want die Nederlander had bij het aanvragen al testaanvraag 1 uitstaan.
        assert_eq!(
            prioriteitswachtrij.enqueue(&nederlander_1, &testaanvraag_5),
            Some(&testaanvraag_5)
        );
        // Testaanvraag 6 is een Nederlander die al een testaanvraag heeft uitstaan, maar hem nu opnieuw doet met een beroep dat hogere prioriteit heeft.
        let testaanvraag_6 = Testaanvraag {
            beroepsgroep: Beroepsgroep::Bg3,
            datumtijd: datetime_nu,
        };
        // De oude prioriteit (`testaanvraag_3`) wordt teruggegeven, dus de prioriteit is nu verhoogd.
        assert_eq!(
            prioriteitswachtrij.enqueue(&nederlander_3, &testaanvraag_6),
            Some(&testaanvraag_3)
        );
        dbg!(&prioriteitswachtrij);
        assert_eq!(prioriteitswachtrij.dequeue(), Some(&nederlander_2));
        assert_eq!(prioriteitswachtrij.dequeue(), Some(&nederlander_4));
        assert_eq!(prioriteitswachtrij.dequeue(), Some(&nederlander_3));
        assert_eq!(prioriteitswachtrij.dequeue(), Some(&nederlander_1));
        assert_eq!(prioriteitswachtrij.dequeue(), None);
    }
}
